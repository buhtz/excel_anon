#!/usr/bin/env python3
import sys
import string
import os.path
import math
import random
import docopt
import openpyxl

"""
    A simple helper script (Python3) to modify an excel data sheet and
    anonymize a specific column.
"""

__author__ = 'Christian Buhtz'
__date__ = 'May 2021'
__maintainer__ = __author__
__email__ = 'c.buhtz@posteo.jp'
__web__ = 'https://codeberg.org/buhtz/excel_anon'
__author_web__ = 'https://codeberg.org/buhtz/'
__license__ = 'GPLv3-or-later'
__version__ = '0.0.1a'
__app_name__ = 'Excel Anonymize'

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

docopt_string = """Excel Anonymize

Usage:
    excel_anon <excel-datei> [<spalte>] [<zeile>] [--sheet=<name>] [--keep] [-h | --help]

Options:
    excel-datei         Name der Excel-Datei.
    spalte              Zu anonyimiserende Spalte (in Excel Notation; d.h. Buchstaben). [default: 'A']
    zeile               Start Zeile (in Excel Notation). [default: 1]
    --sheet=<name>      Name der Arbeitsmappe.
    --keep              Die Real-IDs werden nicht gelöscht. Die Pseudo-IDs werden in einer zusätzlichen Spalte erzeugt.
    -h --help           Dieser Hilfe-Text.
"""


def unique_combinations(chars, length):
    """Berechnet die Anzahl möglicher eindeutiger Kombinationen bei
    einer gegebenen Länge.

    Args:
        chars(string): Zeichsatz verfügbarer Zeichen.
        length(int): Länge des Kombinationsstrings.

    Return:
        (int): Anzahl möglicher Kombination.
    """
    # solution supported by
    # https://medium.com/i-math/combinations-permutations-fa7ac680f0ac
    return (
        math.factorial(len(chars)) /
        (math.factorial(length) *
         math.factorial((len(chars)-length)))
    )


def compute_min_id_length(chars, cases):
    """Berechnet die Mindeslänge einer Fall-ID.

    Args:
        chars (list(string)): Für die ID verfügbarer Zeichenvorrat.
        cases (int): Anzahl der Fälle.

    Returns:
        (int): Länge der ID.
    """
    result = 0

    while True:
        result += 1
        combs = unique_combinations(chars, result)
        if combs > cases:
            return result


def random_id_string(charset, length):
    """Returns a random string of length 'length' with 'charset'.

    Args:
        charset (string): Chars used by the ID.
        length (int): Length of the string.

    Returns:
        (string): The final string.
    """
    return ''.join(random.choice(charset) for i in range(length))


if __name__ == '__main__':
    args = docopt.docopt(docopt_string)

    # Open workbook and sheet
    wb = openpyxl.load_workbook(args['<excel-datei>'])

    if len(wb.worksheets) > 1:
        # select sheet by name
        try:
            sheetname = args['--sheet']
        except KeyError:
            print('Die Excel-Datei enthält mehrere Arbeitsmappen.\nBitte '
                  'mit der Option "--sheet=<name>" den Namen der gewünschten '
                  'Arbeitsmappe angeben.\n'
                  '  Excel-Datei: {}\n'
                  '  Enthaltene Arbeitsmappen: "{}"'
                  .format(args['<excel-datei>'], '", "'.join(wb.sheetnames)))
            sys.exit(1)
        else:
            sheet = wb[sheetname]
    else:
        # select the first one and only sheet
        sheet = wb.worksheets[0]

    # status
    print('Benutzte Arbeitsmappe "{}" aus {} mit {} Spalten und {} Zeilen...'
          .format(sheet.title,
                  args['<excel-datei>'],
                  sheet.max_column,
                  sheet.max_row))

    # Spalte bestimmen
    spalte = args['<spalte>']
    if not spalte:
        spalte = 'A'
    spalte_n = string.ascii_uppercase.find(spalte) + 1

    # Zeile bestimmen
    zeile = args['<zeile>']
    if not zeile:
        zeile = 1
    else:
        zeile = int(zeile)

    # status
    print('BEGINNE...\nAnonymisiere Spalte "{}" und beginne in Zeile {}...'
          .format(spalte, zeile))

    # Anonymisierungsspalte
    if args['--keep']:
        sheet.insert_cols(spalte_n+1)
        print('Neue Spalte hinter Spalte "{}" eingefügt.'.format(spalte))

    # De-Pseudonym dictionary indexed by real-id
    depseudo = {}

    # ID Länge bestimmen
    id_charset = string.ascii_uppercase + string.ascii_uppercase
    id_length = compute_min_id_length(id_charset, sheet.max_row) + 1

    # each row
    for cell in sheet[spalte]:
        # Erst bei definierter Zeile beginnen.
        if cell.row < zeile:
            continue

        # Liegt für die Real-ID schon eine Pseudo-ID vor?
        try:
            pseudo_id = depseudo[cell.value]
        except KeyError:
            while True:
                pseudo_id = random_id_string(id_charset, id_length)
                # be sure this is unique
                if pseudo_id not in depseudo:
                    # remember this new Pseudo-ID
                    depseudo[cell.value] = pseudo_id
                    break

        # BEACHTE!!! Besonderheit bei openpyxl!!!
        # row-index beginnt immer bei 1
        # column-index jedoch bei 0

        if args['--keep']:
            # Pseudo-ID in Nachbarzelle (rechte Spalte) setzen
            sheet[cell.row][cell.column].value = pseudo_id
        else:
            # Real-ID mit Pseudo-ID ersetzen
            cell.value = pseudo_id

    # Neuer Dateiname
    fn, extension = os.path.splitext(args['<excel-datei>'])
    fn = '{}_anonymisiert{}'.format(fn, extension)
    wb.save(fn)
    print(f'Anonymisierte Excel-Datei "{fn}" erstellt.\n...FERTIG.')
