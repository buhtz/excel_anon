# Excel Anonymize

A simple helper script (Python3) to modify an excel data sheet and anonymize a specific column without loosing the assignments.

In the following example table the A column `ID` consist of numbers identifying indivudal persons. The `ID`s can appear in more than one line - e.g. `202`.

| ID 	| Data  	|
|-----	|-------	|
| 101 	| Lore  	|
| 202 	| Ipsum 	|
| 303 	| dolor 	|
| 202 	| sit   	|

The script create random strings (`AnonID`) but retain the assignments. In this example the new created `AnonID` for `ID=202` are equal in line 3 and 5.

| ID 	| AnonID| Data  	|
|-----	|-------|---------
| 101 	| T42 	| Lore  	|
| 202 	| 8UN 	| Ipsum 	|
| 303 	| 9SQ 	| dolor 	|
| 202 	| 8UN 	| sit   	|

The original `ID` column will be removed. So the real result will look like this.

| AnonID| Data  	|
|-----	|-------	|
| T42 	| Lore  	|
| 8UN 	| Ipsum 	|
| 9SQ 	| dolor 	|
| 8UN 	| sit   	|

## Why is it usefull?

In research or other data drivin projects you have sometimes access to personified data. This is a problem because of ethics and the law. If there is no need to identify the person for your research question you have to anonymize the data.

## Usage

```
Usage:
    excel_anon <excel-datei> [<spalte>] [<zeile>] [--sheet=<name>] [--keep] [-h | --help]

Options:
    excel-datei         Name der Excel-Datei.
    spalte              Zu anonyimiserende Spalte (in Excel Notation; d.h. Buchstaben). [default: 'A']
    zeile               Start Zeile (in Excel Notation). [default: 1]
    --sheet=<name>      Name der Arbeitsmappe.
    --keep              Die Real-IDs werden nicht gelöscht. Die Pseudo-IDs werden in einer zusätzlichen Spalte erzeugt.
    -h --help           Dieser Hilfe-Text.
```
